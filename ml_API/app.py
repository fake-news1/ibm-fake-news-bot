from model import *
from preprocessing import *
from Word2Vec import *
import flask
from flask import request, jsonify
import json
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True
model=Fake()
model.load_weights("fake.h5")

@app.route('/', methods=['GET'])
def home():
    return '''<h1>Get links</h1>
<p>A prototype API for google links</p>'''



@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


def predict(query, corpus):
    results = {
        0: 'disagree',
        1: 'agree',
        2: 'discuss',
        3: 'unrelated',
    }
    
    query = reframe_sentence(query, text_processor)
    corpus = reframe_sentence(corpus, text_processor)

    query, corpus = get_ids(query, corpus)

   
    predictions=model.predict([query,corpus])
    for prediction in predictions:
        return results[prediction.argmax()]  # this is the final output   "score between 1 to 10"
    # print("FINISH")     

@app.route('/api/text', methods=['GET'])
def api_filter():
    query_parameters = request.args
    id = query_parameters.get('id')
    text = id
    print(text)
    url = "https://api.serphouse.com/serp/live"
    payload = """{"data":{
			"q":" """+ text +""" ",
			"domain": "google.com",
			"loc": "Guwahati,Assam,India",
			"lang": "en",
			"device": "desktop",
			"serp_type": "web",
			"page": "1",
			"verbatim": "0"
    }}"""
    headers = {
    'accept': "application/json",
    'content-type': "application/json",
    'authorization': "Bearer sb8pOHIv2mP4NmGLQtDdgfBEk5JATyRaPciOzBOCrEvm47olWJeO0djbPZsc"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    if response.status_code != 200:
        raise Exception('API response: {}'.format(response.status_code))
    parsed = json.loads(response.text)
    var=parsed['results']
    org=var['results']['organic']
    dd=[]
    url=[]
    size=0
    for each in org:
        dd.append(each.get('snippet'))
        if len(dd) == size:
            print("empty")
        else:
            size=size+1
            url.append(each.get('link')) 
    count=0
    for corpus in dd:
      count=count+1
    # print(results)
    dictlist = [dict() for x in range(count)]
    x=0
    for corpus in dd:
      answer=predict(text,corpus)
      dictlist[x].update([('Title',corpus),('Link',url[x]),('Score',answer)])
      x=x+1
    # print(dictlist)
    result_json=json.dumps(dictlist)	
    return result_json
    
if __name__ == "__main__":
	
    app.run()

