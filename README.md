# IBM Fake News Bot

**In today’s century, when someone shows us the news, video or pictures of any event, how can we believe its authenticity? It seems that the public is losing their trust in media due to the lack of reliable reference of facts.**

* In today’s century, the free access to create and share information on social media platforms like Facebook, Instagram and other digital platforms has popped out a new problem of fake information, which created rumors around the world. And with advances in technologies like AI, digital animations and social engineering, the line between fake and authentic content is only going to blur even more.

* The hoaxes or false stories can destroy the reputation of any human or industries including politics, health, stock, sports and Finance.



## Motivation

* Recent incident in Mumbai which caused a huge crowd at Railway Station due to Fake News.
* Migrant workers hoping to get back home had gathered at the station.They had hoped that the lockdown would end, but it has been extended till May 3.
* For example, in 2016, BuzzFeed News classified the most viral fake news on Facebook was “Obama had signed an executive order while banning the pledge of allegiance in schools nationwide.
  The content was intended to destroy the Obama’s position during the 2016 US elections
* Fake news can affect the nation as well as international relations. In 2017, Qatar’s state news agency declared that its Twitter account had been hacked and hackers published hoax comments to criticize aspects of the Arab Gulf and US foreign policy towards Iran.
*  Due to the false comments, neighboring countries like Bahrain, the United Arab Emirates, Saudi Arabia and Egypt broke diplomatic ties with Qatar.



## Tech/framework used

- Keras
- Flask
- Pytorch
- Write Frameworks used

## Features



## Showcase

Please provide some screenshots/GIFs.

## Future tasks

List all Future Works

- [ ] Implement XLNET, GPT-2, Roberta, Universal Sentence Encoder
- [ ] Second task
- [ ] Text to Speech
- [ ] Regional Languages support in the Bot


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Credits/Authors
- [Parth Halani](https://github.com/p1halani) (Pre-final Year B.Tech. at IIIT Guwahati)
- Ankit Jaglan (Pre-final Year B.Tech. at IIIT Guwahati)
- [Gargee](https://github.com/Gargee-srivastava) (Pre-final Year B.Tech. at IIIT Guwahati)
- Ashutosh (2nd Year B.Tech. at IIIT Guwahati)
- Sahil Bajaj (Pre-final Year B.Tech. at IIIT Guwahati)
- Saurav Bhartia (Pre-final Year B.Tech. at IIIT Guwahati)