from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
from time import gmtime, strftime
from utils import fetch_reply
import requests
from twilio.twiml.messaging_response import MessagingResponse
import datetime
import random
import requests
import json

app = Flask(__name__)

@app.route("/")
def hello():
    return "Just a cute little hello from Miss Gargee to you "

@app.route("/sms", methods=['POST'])
def sms_reply():
    """Respond to incoming calls with a simple text message."""
    # Fetch the message
    msg = request.form.get('Body').lower()
    resp = MessagingResponse()
    responded = False
    if '2' in msg:
      r = requests.get('https://coronavirus-19-api.herokuapp.com/countries/india')
      if r.status_code == 200:
       data = r.json()
       text = f'_Covid-19 Cases in India_ \n..........................\nConfirmed Cases : *{data["cases"]}* \n................\nToday Cases : *{data["todayCases"]}* \n..............\nDeaths : *{data["deaths"]}* \n..................................\nRecovered : *{data["recovered"]}* \n\n..................\nTotal Tested : *{data["totalTests"]}* \n\n Type 0 to return to main menu'
      else:
       text = 'I could not retrieve the results at this time, sorry.'
      resp.message(text)
      responded = True 
    
    elif '3' in msg:
    
     resp.message("wait we will fetch your results soon!!")
     url = "http://3.6.143.166:5000/api/text?id="
     ms=str(msg)
     #a,b=ms.split(' ',1)
     url=url+ms
     response=requests.get(url)
     parsed=json.loads(response.text)
     s1=""
     for each in parsed:
      s1=s1+each['url']+"\n ................\n"
      if count>5:
       break
      count=count+1
     resp.message(s1)
     responded==True
    elif msg=='news' or msg=='5':
     
     url="""https://newsapi.org/v2/top-headlines?sources=bbc-news,cnn,cnbc,abc-news,google-news-uk,independent&apiKey=3ff5909978da49b68997fd2a1e21fae8"""
     r = requests.get(url)
     #resp.message("stay")       
     if r.status_code == 200:
      resp.message("stay here with us! We are fetching news for you ")
      data = r.json()
      articles = data['articles'][:5]
      result = ""
      ctr=0          
      for article in articles:
     #  if ctr>10:
      #   break
     #  ctr=ctr+1
       title = article['title']
       url = article['url']
       if 'Z' in article['publishedAt']:
        published_at = datetime.datetime.strptime(article['publishedAt'][:19], "%Y-%m-%dT%H:%M:%S")
       else:
        published_at = datetime.datetime.strptime(article['publishedAt'], "%Y-%m-%dT%H:%M:%S%z")
       
       result += """*{}*
Read more: {}
_Published at {:02}/{:02}/{:02} {:02}:{:02}:{:02} UTC_
""".format(
    title,
    url, 
    published_at.day, 
    published_at.month, 
    published_at.year, 
    published_at.hour, 
    published_at.minute, 
    published_at.second
    )+"\n ..................\n"

     else:
      result = 'I cannot fetch news at this time. Sorry!'

     resp.message(result)
     responded = True	
    else:
     phone_no = request.form.get('From')
     reply = fetch_reply(msg, phone_no)

     resp = MessagingResponse()
     resp.message(reply)
     responded = True

     	

    return str(resp)

if __name__ == "__main__":
    app.run(debug=True)
