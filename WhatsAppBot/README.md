 
**2.  ADVANCE WHATSAPP/SLACK CHAT-BOT FOR CRISIS COMMUNICATION**


**To manage massive increase in demand for information during a time of crisis, Inefficient and delayed information dissemination.**


**Technology Used:**


Python, Node.Js, Twilio, Dialog Flow, Google Cloud, Heroku,APIs using FLASK

* As Whatsapp already has more than 400 million users in India , and a very big platform for Information Spread , we are providing a platform for users to help them get all the necessary details.
* We will also be providing a Fake-News Checker on this Chatbot that we have developed.
* We will be providing all the verified information to users in India.
* We will also be keeping a track of all the most popular or common rumour/Fake-News and keep users updated with it on a daily basis.

**Our Unique Features:**
 
* We have also integrated our fact news detector with our whatsapp bot for easy to use service.
* No need to download another app.
* Help in verification of myths and rumors(text/image/video) circulated using our own fact checking techniques(ML/API etc)
* fact checking of input links/texts etc and reporting mechanism.
* We are using sources of known credibility, Natural Language processing and machine learning models to detect fake contents and provide warnings about the presence of objectionable contents .




 **Our Future Plannings:**
 

1. * [ ] Better self-assessment for COVID-19 than the existing iterative chatbot.
1. * [ ] Latest Update and Alerts on Coronavirus. (TRACK CASES NEARBY YOU)
1. * [ ] Apply for Travel Passes for Outside Visits(medical reasons,intercity travel) in case of emergency.
1. * [ ] Rescue over messaging facility
1. * [ ] communication systems today are rudimentary and one-way.Ambulance facility on location as phone lines are misused and not reliable.
1. * [ ] Alerts and information about the lockdown and Hotspot areas.
1. * [ ] Availability of beds/resources in the nearby health facilities and isolation centres to avoid rush in hospitals.
1. * [ ] Connect to doctors online using a helpdesk for guidance.
1. * [ ] Connect the client with the nearby hospital’s doctor dedicated to this purpose (for Special Areas).
1. * [ ] Local language support.
1. * [ ] Can view ML forecasting of effects and Infection risks .
1. * [ ] Educate the general public and to increase awareness and how it can be tackled.
1. * [ ]  Direct link to Pmcares fund donation using upi.
1. * [ ]  Effective Outreach-We can try to get help for those who need help like sort of food and money during crisis as there are many poor people who are not in BPL so they will not get free food or may be they are stuck somewhere so they can’t do much from there for their living. 
1. * [ ] Increase the reach of India’s food-based safety net to ensure that no people are sleeping empty stomach. Food on one message.
1. * [ ] We will attach that user with the nearby food distribution centre.
1. * [ ] Information on Nearby Open Shops for Groceries, Vegetables with Open and Close Timings Based on the Location Shared.







