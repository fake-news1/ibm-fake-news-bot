import React from 'react';
import ReactLoading from 'react-loading';
import { Section ,list} from "./generic";

const Loading = () => (
    <Section className="modal">
      {list.map(l => (
       
          <ReactLoading type={l.prop} color="#000000" />
        
     
      ))}
    </Section>
  );
  
export default Loading;