import axios from 'axios';
import React, { Component } from 'react';
import { Button,FormControl,Form,Container,InputGroup,Dropdown,Alert } from 'react-bootstrap';

import Result from "./result"
import Modal from "./model";
import Source from "./sources"
import Alr from './alert1'
class SearchBox extends Component {

  constructor () {
    super();
    this.state = {
      showModal: false,
      URL:"",
      clicked:false,
      data:[{score:"",Title:""}],
      tr:0,
      fl:0,
      d:0,
      Title:[{}],
      Links:[]
    
    };
  
    this.updateURL = this.updateURL.bind(this);
    this.onsubmit =this.onsubmit.bind(this);
   
   
  }

//https://cors-anywhere.herokuapp.com/http://3.6.143.166:5000/api/text?id=india+cases+1000'


   async onsubmit (){ 
  this.setState({ clicked: false});
    var trr=0,flr=0,dr=0;
    var Ti=[];
    var Li=[];
    var URL1=this.state.URL.replace(/\s/g, '+')
    var URL2="https://cors-anywhere.herokuapp.com/http://3.6.143.166:5000/api/text?id="+URL1;
    console.log(URL2)
    this.setState({ showModal: true });
   await axios.get(URL2)
  .then(function (response) {
    // handle success
   
    for(var i=0;i<response.data.length;i++)
    {

      if(response.data[i].Score==='agree')
      trr++;
      else if(response.data[i].Score==='discuss')
        dr++;
        else if(response.data[i].Score==='disagree')
        flr++;
        else
        { console.log(response.data[i].Score);

        }     
       // Ti.push(response.data[i].Title)
        //Li.push(response.data.url)
        if(i<10){ 
        Ti.push({
           title:response.data[i].Title,
           link:response.data[i].url
        })
      }
    }


    

  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
 
  });
  console.log(trr);
    console.log(flr);
    console.log(dr);
  await this.setState({ tr: trr });
  await this.setState({ fl: flr });
  await this.setState({ d: dr });
  await this.setState({ URL:"" });
  await this.setState({ Title:Ti });
  //await this.setState({ Links:Li });
 
 
  await this.setState({ showModal: false });
  await this.setState({ clicked: true });
  };



   updateURL=(newValue)=>{

        const URL=this.state.URL;
        this.setState({
            
          URL:newValue.target.value
        })
        console.log(URL);
        
      };

     

  render() {
    return (
      
      
      <div className="centr">
        {this.state.clicked===true&&this.state.tr==0&&this.state.fl==0&&this.state.d==0?<Alr />:null
      }
     <InputGroup className="lg" size="lg">
    <FormControl
      placeholder="Fact checker"
      aria-label="Fact checker"
      aria-describedby="basic-addon2"
      value={this.URL}
       
		    onChange={this.updateURL}
    /> <InputGroup.Append>
    
  
  </InputGroup.Append>
  <InputGroup.Append>
  {this.state.showModal==false?<Button onClick={this.onsubmit} variant="warning" size="lg"><i class="fas fa-search"></i>Search</Button>:null}
  </InputGroup.Append>
 
 
</InputGroup>
{this.state.clicked?<Result 
    tr={this.state.tr}
    fl={this.state.fl}
    d={this.state.d}
    
    />:null}

    {this.state.clicked?<Source 
    
    title={this.state.Title}
    
    
    />:null}

<Modal   
  showModal={this.state.showModal}
/>

      </div>
    );
  }
}

export default SearchBox;