
import React, { Component } from 'react';
import { Button,FormControl,Form,Nav,Navbar} from 'react-bootstrap';
class NavBar extends Component {
  render() {
    return (
        <Navbar bg="dark" variant="dark" sticky="top" size="lg">
        <Navbar.Brand href="mailto:gargeeSIH@gmail.com">ContactUS</Navbar.Brand>
        <Nav className="mr-auto">
          
        </Nav>
        <Form inline>
          <Button variant="outline-info">Our Sources<i class="fas fa-link"></i></Button>
        </Form>
      </Navbar>
    );
  }
}

export default NavBar;